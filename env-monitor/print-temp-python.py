#!/usr/bin/env python

import dhtreader
import argparse


PiPin = 18

def read_dht22( Search ):
    dhtreader.init()
    if ( Search is "Temp" ) :
        temp = ""
        while ( not (isinstance(temp, float)) ) :
            output = dhtreader.read(22, PiPin)
            if ( output ) :
                temp = output[0]
        result = temp
    if ( Search is "Hum" ) :
        humid = ""
        while ( not (isinstance(humid, float)) ) :
            output = dhtreader.read(22, PiPin)
            if ( output ) :
                humid = output[1]
        result = humid
    return result
    
def read_dht_temp():
    temp = read_dht22("Temp")
    return temp
    
def read_dht_humid():
    humid = read_dht22("Hum")
    return humid
    
parser = argparse.ArgumentParser(description='Print out temperature or humidity from DHT22 sensor.')
parser.add_argument('type', choices=['temp', 'humid'], type=str,
                   help='Sensor type - Temperature (temp) or Humidity (humid)')
parser.add_argument('-r', '--round', action='store_const', const=bool(1),
                   help='Display as rounded int multiplied by 10 for SNMP')

args = parser.parse_args()

if ( args.type == "humid" ) :
    print read_dht_humid()

if ( args.type == "temp" ) :
    print int(read_dht_temp())