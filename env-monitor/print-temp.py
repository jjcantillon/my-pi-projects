#!/usr/bin/env python

import re
import subprocess

PiPin = 18

def read_dht22( Search ):
    output = subprocess.check_output(["./../Adafruit_DHT_Driver/Adafruit_DHT", "22", str(PiPin)])
    matches = re.search(Search + " =\s+([0-9.]+)", output)
    if ( matches ):
        result = float(matches.group(1))
        return result
    
def read_dht_temp():
    temp = read_dht22("Temp")
    return temp
    
def read_dht_humid():
    humid = read_dht22("Hum")
    return humid
    
print(read_dht_temp())
print(read_dht_humid())