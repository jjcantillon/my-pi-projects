#!/usr/bin/env python2.7
# Python 2.7 version by Alex Eames of http://RasPi.TV 
# functionally equivalent to the Gertboard leds test by Gert Jan van Loo & Myra VanInwegen
# Use at your own risk - I'm pretty sure the code is harmless, but check it yourself.

import wiringpi
from time import sleep
ports = [25, 24, 23, 22, 21, 18, 17, 11, 10, 9, 8, 7]   # define ports list
ports_rev = ports[:]                                    # make a copy of ports list
ports_rev.reverse()                                     # and reverse it as we need both

wiringpi.wiringPiSetupGpio()                            # initialise wiringpi

for port_num in ports:
    wiringpi.pinMode(port_num, 1)                       # set up ports for output

def led_drive(reps, multiple, direction):               # define function to drive
    for i in range(reps):                               # repetitions, single or multiple
        for port_num in direction:                      # and direction
            wiringpi.digitalWrite(port_num, 1)
            sleep(0.11)
            if not multiple:
                wiringpi.digitalWrite(port_num, 0)
                                                        # Print Wiring Instructions
print "These are the connections for the LEDs test:"
print "jumpers in every out location (U3-out-B1, U3-out-B2, etc)"
print "GP25 in J2 --- B1 in J3 \nGP24 in J2 --- B2 in J3"
print "GP23 in J2 --- B3 in J3 \nGP22 in J2 --- B4 in J3"
print "GP21 in J2 --- B5 in J3 \nGP18 in J2 --- B6 in J3"
print "GP17 in J2 --- B7 in J3 \nGP11 in J2 --- B8 in J3"
print "GP10 in J2 --- B9 in J3 \nGP9 in J2 --- B10 in J3"
print "GP8 in J2 --- B11 in J3 \nGP7 in J2 --- B12 in J3"
print "(If you don't have enough straps and jumpers you can install"
print "just a few of them, then run again later with the next batch.)"
raw_input("When ready hit enter.\n>")

try:                                                    # Call the led driver function
    led_drive(3, 0, ports)                              # for each required pattern
    led_drive(1, 0, ports_rev)
    led_drive(1, 0, ports)
    led_drive(1, 0, ports_rev)
    led_drive(1, 1, ports)
    led_drive(1, 0, ports)        
    led_drive(1, 1, ports)
    led_drive(1, 0, ports)
except KeyboardInterrupt:                               # trap a CTRL+C keyboard interrupt
    for port_num in ports:                              # so it switches off all LEDs and ports
        wiringpi.digitalWrite(port_num, 0)
